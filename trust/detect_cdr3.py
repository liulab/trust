"""
CDR3 detection and annotation
"""

import sys,re,os
from Bio import pairwise2
import numpy as np
try:
    import parasail
    SEQ_SUB = parasail.matrix_create('ABCDEFGHIJKLMNOPQRSTUVWXYZ*', 1, -1)
    SEQ_SUB_DNA = parasail.matrix_create('atcg', 1, -1)
    USE_PARASAIL = True
except:
    print '''Please install parasail. If not, use BioPython instead.'''
    from Bio import pairwise2
    pairwise2.MAX_ALIGNMENTS = 1
    USE_PARASAIL = False

from trust.process_seq import *
from trust.find_overlap_seq import *


curDir=os.path.dirname(os.path.realpath(__file__))+'/'
#curDir='./'


AAcode={"TTT":"F","TTC":"F","TTA":"L","TTG":"L",
        "TCT":"S","TCC":"S","TCA":"S","TCG":"S",
        "TAT":"Y","TAC":"Y","TAA":"*","TAG":"*",
        "TGT":"C","TGC":"C","TGA":"*","TGG":"W",
        "CTT":"L","CTC":"L","CTA":"L","CTG":"L",
        "CCT":"P","CCC":"P","CCA":"P","CCG":"P",
        "CAT":"H","CAC":"H","CAA":"Q","CAG":"Q",
        "CGT":"R","CGC":"R","CGA":"R","CGG":"R",
        "ATT":"I","ATC":"I","ATA":"I","ATG":"M",
        "ACT":"T","ACC":"T","ACA":"T","ACG":"T",
        "AAT":"N","AAC":"N","AAA":"K","AAG":"K",
        "AGT":"S","AGC":"S","AGA":"R","AGG":"R",
        "GTT":"V","GTC":"V","GTA":"V","GTG":"V",
        "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
        "GAT":"D","GAC":"D","GAA":"E","GAG":"E",
        "GGT":"G","GGC":"G","GGA":"G","GGG":"G"}

def ParseFa(fname):
    InputStr=open(fname).readlines()
    FaDict={}
    seq=''
    for line in InputStr:
        if line.startswith('>'):
            if len(seq)>0:
                FaDict[seqHead]=seq
                seq=''
            seqHead=line.strip()
        else:
            seq+=line.strip()
    if seqHead not in FaDict:
        FaDict[seqHead]=seq
    return FaDict

def GetJMotifs(FaDict,PAT):
    JMotifs={}
    for kk in FaDict:
        jj=FaDict[kk]
        for pp in PAT.values():
            mm=re.search(pp,jj)
            if mm is not None:
                break
        if mm is None:
            #print kk,jj
            continue
        mms=mm.span()
        kkJ=jj[mms[0]-3:mms[1]]
        if kkJ not in JMotifs:
            JMotifs[kkJ]=[kk]
        else:
            JMotifs[kkJ].append(kk)
    return JMotifs

def GetVMotifs(FaDict,PAT):
    VMotifs={}
    VmotifList=[]
    for kk in FaDict:
        vv=FaDict[kk]
        for pp in PAT.values():
            mm=re.search(pp,vv)
            if mm is not None:
                break
        if mm is None:
        #    print kk,vv
            continue
        mms=mm.span()
        kkV=vv[mms[0]-3:mms[1]] ## Keep at least 3 flanking amino acids
        VmotifList.append(kkV)
    for kk in FaDict:
        vv=FaDict[kk]
        for kkV in VmotifList:
            if kkV in vv:
                if kkV not in VMotifs:
                    VMotifs[kkV]=[kk]
                else:
                    VMotifs[kkV].append(kk)
    for kk in VMotifs:
        vv=VMotifs[kk]
        vv=list(set(vv))
        VMotifs[kk]=vv
    return VMotifs


def GetCDR3Patterns(genome, chain='total'):
    
    if genome == "mm10":

        TRAV = {'AV0':'Y[YFL]C[AV]', 'AV1':'YICV', 'AV2':'AMYF', 'AV3':'Y[YH]CI', 'AV4':'CYCA', 'AV5':'YFCSA', 'AV6':'CA[AVLMISP]'}
        TRBV = {'BV0':'CASS', 'BV1':'CAW', 'BV2':'YF[GW]ASS', 'BV3':'CSS[SR]', 'BV4':'Y[FL]CA', 'BV5':'CSA', 'BV6':'YLCGA'}
        TRGV = {'GV0':'YYC', 'GV1':'STLK', 'GV2':'CSY', 'GV3':'CA[VDCS]'}
        TRDV = {'DV0':'Y[YFL]CA', 'DV1':'CIL', 'DV2':'CG[GS]','DV3':'CA[AMILS]'}
        IGHV = {'HV0':'Y[YF]C.R', 'HV1':'YCCARQV', 'HV2':'YY[FSR]AR', 'HV3':'Y[YFH]C[ATK]', 'HV4':'YLCKE', 
                'HV5':'YCAR', 'HV6':'YYCNA', 'HV7':'YYCPY', 'HV8':'YYCV', 'HV9':'YFC[SM]R', 'HV10':'YYWAR', 
                'HV11':'YYCSREN', 'HV12':'YYY[SP]REN', 'HV13':'HFCAR', 'HV14':'YFC', 'HV15':'A[NY]MEL', 'HV16':'AYMQL', 
                'HV17':'DTAT', 'HV18':'YYGAW'}
        IGKV = {'KV0':'Y[YFH]C', 'KV1':'YYLQ', 'KV2':'YYF', 'KV3':'CEQS', 'KV4':'DVAT', 'KV5':'C[QLSFW]Q', 'KV6':'YCQ'}
        IGLV = {'LV0':'YS[NT]HF', 'LV1':'VQAE', 'LV2':'IKEQFV', 'LV3':'IRGQFV'}


        TRAJ = {'AJ0':'FG.G', 'AJ1':'LG[AK]G', 'AJ2':'WG[SL]G', 'AJ3':'FSSG', 'AJ4':'FATG', 'AJ5':'FREL', 
                'AJ6':'CGLG', 'AJ7':'LV[SL]G', 'AJ8':'NYKL', 'AJ9':'NTSS', 'AJ10':'FFFG', 'AJ11':'FGAE',  'AJ12':'FGTW'}#, 'AJ13':'.[LV].F'}
        TRBJ = {'BJ0':'FG.G', 'BJ1':'FAAG', 'BJ2':'HGLG', 'BJ3':'TDWQ'}
        TRGJ = {'GJ0':'FA[EK]G', 'GJ1':'SGF', 'GJ2':'WDF', 'GJ3':'GTSW'}
        TRDJ = {'DJ0':'FG[TQ]G', 'DJ1':'[DL]VF', 'DJ2':'RQMF'}
        IGHJ = {'HJ0':'WG.G'}
        IGKJ = {'KJ0':'FG.G', 'KJ1':'FSDG', 'KJ2':'TFG'}
        IGLJ = {'LJ0':'FG[SG]G','LJ1':'FSSNG'}


    else:

        TRAV = {'AV0':'Y[YFL]C[AVG]', 'AV1':'AMYF', 'AV2':'YYC[LI]', 'AV3':'YFC', 
                'AV4':'YLC', 'AV5':'CA[LVMATFG]', 'AV6':'CVV', 'AV7':'CL[LV]', 'AV8':'CI[AL]',
                'AV9':'TLNQ', 'AV10':'LYLT', 'AV11':'SVQW', 'AV12':'LEEKG', 'AV13':'RKSST', 'AV14':'LQIT'}
        TRBV = {'BV0':'CAS[ST]', 'BV1':'CTSS', 'BV2':'VYFC', 'BV3':'CS[AT]', 'BV4':'CA[WSIT]', 'BV5':'YLCS',
                'BV6':'RASS', 'BV7':'QTSV'}
        TRGV = {'GV0':'YYCA', 'GV1':'CA[TL]W'}
        TRDV = {'DV0':'Y[YF]CA', 'DV1':'[YF]CA'}
        IGHV = {'VB1':"Y[YF]C[AVT]",'VB2':"YCCA",'VB3':'YCAR','VB4':'DTA[TV]{1}YY','VB5':'Y[HF]CA','VB6':'DYCA','VB7':'VYCC',
                'VB8':'RSDDT','VB9':"LSSLR",'VB10':'DTSKN','VB11':'DSKNS','VB12':'LKAED','VB13':'KASDT'
                }
        IGKV = {'VL1':'Y[YF]C','VL':'YYF'}
        IGLV = {'VL1':'Y[YF]C','VL':'YYF'}

        TRAJ = {'AJ0':'FG.G', 'AJ1':'WG[AL]G', 'AJ2':'FARG'}
        TRBJ = {'BJ0':'FG.G', 'BJ1':'VGPG'}
        TRGJ = {'GJ0':'FG.G', 'GJ1':'FA[EK]G'}
        TRDJ = {'DJ0':'FG.G'}
        IGHJ = {'JB':"WG.G"}
        IGKJ = {'JL':'FG.G'}
        IGLJ = {'JL':'FG.G'}
    #print TRAV, TRBV, TRGV, TRDV, IGHV, IGKV, TRAJ, TRBJ, TRGJ, TRDJ, IGHJ, IGKJ
    curDir=os.path.dirname(os.path.realpath(__file__))+'/'
    AVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRAV-imgt-AA.fa")
    AJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRAJ-imgt-AA.fa")
    BVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRBV-imgt-AA.fa")
    BJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRBJ-imgt-AA.fa")
    DVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRDV-imgt-AA.fa")
    DJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRDJ-imgt-AA.fa")
    GVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRGV-imgt-AA.fa")
    GJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/TRGJ-imgt-AA.fa")

    AVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRAV-imgt-DNA.fa")
    AJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRAJ-imgt-DNA.fa")
    BVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRBV-imgt-DNA.fa")
    BJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRBJ-imgt-DNA.fa")
    DVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRDV-imgt-DNA.fa")
    DJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRDJ-imgt-DNA.fa")
    GVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRGV-imgt-DNA.fa")
    GJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/TRGJ-imgt-DNA.fa")

    bHVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGHV-imgt-AA.fa")
    bHJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGHJ-imgt-AA.fa")
    bHVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGHV-imgt-DNA.fa")
    bHJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGHJ-imgt-DNA.fa")
    bHCFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGHC-imgt-DNA.fa")

    bKVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGKV-imgt-AA.fa")
    bKJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGKJ-imgt-AA.fa")
    bKVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGKV-imgt-DNA.fa")
    bKJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGKJ-imgt-DNA.fa")

    bLVFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGLV-imgt-AA.fa")
    bLJFaDict=ParseFa(curDir+"data/"+genome+"/Reference/IGLJ-imgt-AA.fa")
    bLVFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGLV-imgt-DNA.fa")
    bLJFaDict_DNA=ParseFa(curDir+"data/"+genome+"/Reference/IGLJ-imgt-DNA.fa")

    if chain=='Alpha': return TRAV, TRAJ, AVFaDict, AJFaDict#, AVFaDict_DNA, AJFaDict_DNA
    elif chain=='Beta': return TRBV, TRBJ, BVFaDict, BJFaDict#, BVFaDict_DNA, BJFaDict_DNA
    elif chain=='Gamma': return TRGV, TRGJ, GVFaDict, GJFaDict#, GVFaDict_DNA, GJFaDict_DNA
    elif chain=='Delta': return TRDV, TRDJ, DVFaDict, DJFaDict#, DVFaDict_DNA, DJFaDict_DNA
    elif chain=='Heavy': return IGHV, IGHJ, bHVFaDict, bHJFaDict, bHCFaDict_DNA#, bHVFaDict_DNA, bHJFaDict_DNA, bHCFaDict_DNA
    elif chain=='Light_L': return IGLV, IGLJ, bLVFaDict, bLJFaDict#, bLVFaDict_DNA, bLJFaDict_DNA
    elif chain=='Light_K': return IGKV, IGKJ, bKVFaDict, bKJFaDict#, bKVFaDict_DNA, bKJFaDict_DNA
    else:

        MotifDict={'BJ':GetJMotifs(BJFaDict, TRBJ),'BV':GetVMotifs(BVFaDict, TRBV),
           'DJ':GetJMotifs(DJFaDict, TRDJ),'DV':GetVMotifs(DVFaDict, TRDV),
           'AJ':GetJMotifs(AJFaDict, TRAJ),'AV':GetVMotifs(AVFaDict, TRAV),
           'GJ':GetJMotifs(GJFaDict, TRGJ),'GV':GetVMotifs(GVFaDict, TRGV)}
        DNAFaDict={'BJ':BJFaDict_DNA,'BV':BVFaDict_DNA,
                   'DJ':DJFaDict_DNA,'DV':DVFaDict_DNA,
                   'AJ':AJFaDict_DNA,'AV':AVFaDict_DNA,
                   'GJ':GJFaDict_DNA,'GV':GVFaDict_DNA}
        MotifDictB={'HJ':GetJMotifs(bHJFaDict,IGHJ),'HV':GetVMotifs(bHVFaDict,IGHV)}
        DNAFaDictB={'HJ':bHJFaDict_DNA,'HV':bHVFaDict_DNA}

        MotifDictL={'KJ':GetJMotifs(bKJFaDict,IGKJ),'KV':GetVMotifs(bKVFaDict,IGKV),
                    'LJ':GetJMotifs(bLJFaDict,IGLJ),'LV':GetVMotifs(bLVFaDict,IGLV)}
        DNAFaDictL={'KJ':bKJFaDict_DNA,'KV':bKVFaDict_DNA,'LJ':bLJFaDict_DNA,'LV':bLVFaDict_DNA}

        return MotifDict, DNAFaDict, MotifDictB, DNAFaDictB, MotifDictL, DNAFaDictL


def BreakSeqIntoAAcodes(seq,frame=0):
    '''
    Break a sequence into consecutive 3 letters, with arbitrarily selected frameshift bases
    '''
    n=len(seq)
    AAseqs=[]
    i=frame
    while i<n:
        if i+3>n:
            break
        AAseqs.append(seq[i:(i+3)])
        i+=3
    return AAseqs


def TranslateAA(seq):
    AAList=[]
    
    for ff in [0,1,2]:
        AAseqs=BreakSeqIntoAAcodes(seq,frame=ff)
        AA=''
        for i in AAseqs:
            if len(i)!=3:
                continue
            AA+=AAcode[i]
        AAList.append((AA,ff+1))
    
    for ff in [0,1,2]:
        AAseqsRC=BreakSeqIntoAAcodes(ReverseCompSeq(seq),frame=ff)
        AA=''
        for i in AAseqsRC:
            if len(i)!=3:
                continue
            AA+=AAcode[i]
        AAList.append((AA,-ff-1))        
    
    
    return AAList


def DetectCDR3(seq,VFaDict,JFaDict,CDR3patV,CDR3patJ,CFaDict=None,Bcell=False,light_chain=False):

    bestMatchC=''

    if Bcell and not light_chain:
        #print '## Local alignment to determine constant gene'
        MatchList=[]
        ScoreList=[]
        bestMatch=''
        bestScore=0
        for Cgene in CFaDict:
            ss=seq
            ss_rc=ReverseCompSeq(ss).lower()
            ss=ss.lower()
            Cseq=CFaDict[Cgene][0:99]
            if USE_PARASAIL:
                aln = parasail.sw_striped_sat(Cseq, ss, 9, 0, SEQ_SUB_DNA)
                if aln.score > bestScore:
                    bestMatch = Cgene
                    bestScore = aln.score

                aln_rc = parasail.sw_striped_sat(Cseq, ss_rc, 9, 0, SEQ_SUB_DNA)
                if aln_rc.score > bestScore:
                    bestMatch = Cgene
                    bestScore = aln_rc.score

                if aln_rc.score > aln.score:
                    MatchList.append(Cgene)
                    ScoreList.append(aln_rc.score)
                else:
                    MatchList.append(Cgene)
                    ScoreList.append(aln.score)
            else:
                aln=pairwise2.align.localms(Cseq, ss,1,-1,-9,0)
                if len(aln)==0:
                    continue
                else:
                    aln=aln[0]
                if aln[2]>bestScore:
                    bestMatch=Cgene
                    bestScore=aln[2]
                aln_rc=pairwise2.align.localms(Cseq, ss_rc,1,-1,-9,0)
                if len(aln_rc)==0:
                    continue
                else:
                    aln_rc=aln_rc[0]
                if aln_rc[2]>bestScore:
                    bestMatch=Cgene
                    bestScore=aln_rc[2]

                if aln_rc[2] > aln[2]:
                    MatchList.append(Cgene)
                    ScoreList.append(aln_rc[2])
                else:
                    MatchList.append(Cgene)
                    ScoreList.append(aln[2])

        if bestScore<=15:
            bestMatchC=''
        else:
            bestIdx=np.where(np.array(ScoreList)==bestScore)[0]
            bestMatchC='_'.join(list(np.array(MatchList)[bestIdx]))
        #print bestScore
    AAList=TranslateAA(seq)
    '''
    if Bcell:
        if not light_chain:
            CDR3patV0=CDR3patVB
            CDR3patJ0=CDR3patJB
        else:
            CDR3patV0=CDR3patVL
            CDR3patJ0=CDR3patJL
    else:
        CDR3patV0=CDR3patV
        CDR3patJ0=CDR3patJ
    '''    
    CDR3seq=[]
    for AA in AAList:
        AAseq=AA[0]
        if "*" in AAseq:
            continue
        Vflag=0
        Vpos=[]
        for ppk in CDR3patV:
            pp=CDR3patV[ppk]
            tmp=re.search(pp,AAseq)
            if tmp:
                Vpos.append((ppk,tmp.start(), tmp.group(0), tmp.end()-tmp.start()))
                if Vflag==0:
                    Vflag=1
                break
        Jflag=0
        Jpos=[]
        for ppk in CDR3patJ:
            pp=CDR3patJ[ppk]
            tmp=re.search(pp,AAseq)
            if tmp:
                Jpos.append((ppk,tmp.start(), tmp.group(0), tmp.end()-tmp.start()))
                if Jflag==0:
                    Jflag=1
                break

        if Vflag==0 and Jflag==0:
            CDR3=[AA,('','','',''),-1,('','','',''),-1,0,0]


        if Jflag==1 and Vflag==0:
            ## to save time, first check if J gene is correct
            bestMatchList=[]
            bestScoreList=[]
            for jj in Jpos:
                ss=AAseq[max(0,jj[1]-4):]
                Nj=len(ss)
                NN=Nj
                reverse=False
                if Nj>10:
                    reverse=True
                    NN=10
                bestMatch=''
                bestScore=0
                if Nj<6:
                    bestMatchList.append((bestMatch,jj[0], jj[2], jj[3]))
                    bestScoreList.append(bestScore)
                    continue
                for Jgene in JFaDict:
                    Jseq=JFaDict[Jgene]
                    if USE_PARASAIL:
                        aln = parasail.sw_striped_sat(Jseq, ss, 9, 0, SEQ_SUB)
                        if aln.score > bestScore:
                            bestMatch = Jgene
                            bestScore = aln.score
                    else:
                        aln=pairwise2.align.localms(Jseq,ss,1,-1,-9,0)
                        if len(aln)==0:
                            continue
                        else:
                            aln=aln[0]
                        if aln[2]>bestScore:
                            bestMatch=Jgene
                            bestScore=aln[2]
                if bestScore<NN-4:
                    # allow for 2 mismatches or single gaps
                    bestMatch=''
                    bestScore=0
                bestMatchList.append((bestMatch,jj[0], jj[2], jj[3]))
                bestScoreList.append(bestScore)
            bSj=max(bestScoreList)
            if bSj==0:
                CDR3 = [AA,('','','',''),-1,('','','',''),Jpos[0][1],0,0]                       
            else:
                ppj=bestScoreList.index(bSj)
                matched_Jgene=bestMatchList[ppj]
                matched_Jpos=Jpos[ppj]
                Nj=len(AAseq[matched_Jpos[1]:])
                CDR3 = [AA,('','','',''),-1,matched_Jgene,matched_Jpos[1],bSj,Nj]

        if Vflag==1 and Jflag==1:
            bestMatchListJ=[]
            bestScoreListJ=[]
            for jj in Jpos:
                ss=AAseq[max(0,jj[1]-4):]
                Nj=len(ss)
                NN=Nj
                reverse=False
                if Nj>10:
                    reverse=True
                    NN=10
                bestMatch=''
                bestScore=0
                if Nj<6:
                    bestMatchListJ.append((bestMatch,jj[0], jj[2], jj[3]))
                    bestScoreListJ.append(bestScore)
                    continue
                for Jgene in JFaDict:
                    Jseq=JFaDict[Jgene]
                    if USE_PARASAIL:
                        aln = parasail.sw_striped_sat(Jseq, ss, 9, 0, SEQ_SUB)
                        if aln.score > bestScore:
                            bestMatch = Jgene
                            bestScore = aln.score
                    else:
                        aln=pairwise2.align.localms(Jseq,ss,1,-1,-9,0)
                        if len(aln)==0:
                            continue
                        else:
                            aln=aln[0]                            
                        if aln[2]>bestScore:
                            bestMatch=Jgene
                            bestScore=aln[2]
                if bestScore<NN-4:
                    # allow for 2 mismatches or single gaps
                    bestMatch=''
                    bestScore=0
                bestMatchListJ.append((bestMatch,jj[0], jj[2], jj[3]))
                bestScoreListJ.append(bestScore)
            bSj=max(bestScoreListJ)
            bestMatchListV=[]
            bestScoreListV=[]
            for vv in Vpos:
                ss=AAseq[0:(vv[1]+3)]
                Nv=len(ss)
                bestMatch=''
                bestScore=0
                if Nv<4:
                    bestMatchListV.append((bestMatch,vv[0], vv[2], vv[3]))
                    bestScoreListV.append(bestScore)
                    continue
                matched_vgenes=[]
                vscores=[]
                for Vgene in VFaDict:
                    Vseq=VFaDict[Vgene][-(Nv+10):]
                    if USE_PARASAIL:
                        aln = parasail.sw_striped_sat(Vseq, ss, 9, 0, SEQ_SUB)
                        if aln.score > bestScore:
                            bestMatch = Vgene
                            bestScore = aln.score
                        matched_vgenes.append(Vgene)
                        vscores.append(aln.score)
                    else:
                        aln=pairwise2.align.localms(Vseq,ss,1,-1,-9,0)
                        if len(aln)==0:
                            continue
                        else:
                            aln=aln[0]
                        if aln[2]>bestScore:
                            bestMatch=Vgene
                            bestScore=aln[2]
                        matched_vgenes.append(Vgene)
                        vscores.append(aln[2])
                if bestScore<Nv-10:
                    # allow for 5 mismatches or single gaps
                    bestMatch=''
                    bestScore=0
                bestIdx=np.where(np.array(vscores)==bestScore)[0]
                bestMatch='_'.join(list(np.array(matched_vgenes)[bestIdx]))
                bestMatchListV.append((bestMatch,vv[0], vv[2], vv[3]))
                bestScoreListV.append(bestScore)                        
            bSv=max(bestScoreListV)
            if bSv==0 and bSj==0:
                CDR3=[AA,('','','',''),Vpos[0][1],('','','',''),Jpos[0][1],0,0]
            if bSv>0 and bSj==0:
                ppv=bestScoreListV.index(bSv)
                matched_Vgene=bestMatchListV[ppv]
                matched_Vpos=Vpos[ppv]
                Nv=len(AAseq[0:matched_Vpos[1]])
                CDR3=[AA,matched_Vgene,matched_Vpos[1],('','','',''),Jpos[0][1],bSv,Nv]
            if bSv==0 and bSj>0:
                ppj=bestScoreListJ.index(bSj)
                matched_Jgene=bestMatchListJ[ppj]
                matched_Jpos=Jpos[ppj]
                Nj=len(AAseq[matched_Jpos[1]:])
                CDR3 = [AA,('','','',''),Vpos[0][1],matched_Jgene,matched_Jpos[1],bSj,Nj]
            if bSv>0 and bSj>0:
                ppj=bestScoreListJ.index(bSj)
                matched_Jgene=bestMatchListJ[ppj]
                matched_Jpos=Jpos[ppj]
                Nj=len(AAseq[matched_Jpos[1]:])
                ppv=bestScoreListV.index(bSv)
                matched_Vgene=bestMatchListV[ppv]
                matched_Vpos=Vpos[ppv]
                Nv=len(AAseq[0:matched_Vpos[1]])
                CDR3 = [AA,matched_Vgene,matched_Vpos[1],matched_Jgene,matched_Jpos[1],bSv+bSj,Nv+Nj]


        if Vflag==1 and Jflag==0:
            ## Vflag=1,Jflag=0
            bestMatchList=[]
            bestScoreList=[]
            for vv in Vpos:
                ss=AAseq[0:(vv[1]+3)]
                Nv=len(ss)
                bestMatch=''
                bestScore=0
                if Nv<4:
                    bestMatchList.append((bestMatch,vv[0], vv[2], vv[3]))
                    bestScoreList.append(bestScore)
                    continue
                matched_vgenes=[]
                vscores=[]
                for Vgene in VFaDict:
                    Vseq=VFaDict[Vgene][-(Nv+10):]
                    if USE_PARASAIL:
                        aln = parasail.sw_striped_sat(Vseq, ss, 9, 0, SEQ_SUB)
                        if aln.score > bestScore:
                            bestMatch = Vgene
                            bestScore = aln.score
                        matched_vgenes.append(Vgene)
                        vscores.append(aln.score)
                    else:
                        aln=pairwise2.align.localms(Vseq,ss,1,-1,-9,0)
                        if len(aln)==0:
                            continue
                        else:
                            aln=aln[0]
                        if aln[2]>bestScore:
                            bestMatch=Vgene
                            bestScore=aln[2]
                        matched_vgenes.append(Vgene)
                        vscores.append(aln[2])
                if bestScore<Nv-10:
                    # allow for 5 mismatches or single gaps
                    bestMatch=''
                    bestScore=0
                bestIdx=np.where(np.array(vscores)==bestScore)[0]
                bestMatch='_'.join(list(np.array(matched_vgenes)[bestIdx]))
                bestMatchList.append((bestMatch,vv[0], vv[2], vv[3]))
                bestScoreList.append(bestScore)                        
            bSv=max(bestScoreList)
            if bSv==0:
                CDR3 = [AA,('','','',''),Vpos[0][1],('','','',''),-1,0,0]
            else:
                ppv=bestScoreList.index(bSv)
                matched_Vgene=bestMatchList[ppv]
                matched_Vpos=Vpos[ppv]
                Nv=len(AAseq[0:matched_Vpos[1]])
                CDR3 = [AA,matched_Vgene,matched_Vpos[1],('','','',''),-1,bSv,Nv]

        CDR3seq.append(CDR3)


    newCDR3seq=[]

    for CDR3 in CDR3seq:
        if CDR3[2]==-1 and CDR3[4]==-1:
            continue
        if len(bestMatchC)>0:
            newCDR3=CDR3[0:3]
            newCDR3.append((CDR3[3][0]+'00000'+bestMatchC,CDR3[3][1]))
            newCDR3+=CDR3[4:]
        else:
            newCDR3=CDR3
        newCDR3seq.append(newCDR3)

    return newCDR3seq


def AnnotateCDR3(Seq,pRD,ContigReads=[],light_chain=False,geneType='',error=1,overlap_thr=10,Bcell=False, genome='hg19'):
    
    if Bcell:

        v_trim_start_thro=7

        if not light_chain:
            dna_len_thro=16*3
            CDR3patV, CDR3patJ, VFaDict, JFaDict, CFaDictDNA=GetCDR3Patterns(genome, 'Heavy')
            CDR3seq_all=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ, CFaDict=CFaDictDNA, Bcell=True, light_chain=light_chain)[0:]
        
        else:
            dna_len_thro=13*3
            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Light_K')
            CDR3seqK=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ,Bcell=True,light_chain=light_chain)

            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Light_L')
            CDR3seqL=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ,Bcell=True,light_chain=light_chain)
            CDR3seq_all=CDR3seqK[0:]+CDR3seqL[0:]
    else:

        v_trim_start_thro=5
        dna_len_thro=13*3

        if geneType=='':
            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Alpha')
            CDR3seqA=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)

            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Beta')
            CDR3seqB=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)

            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Delta')
            CDR3seqD=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)

            CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Gamma')
            CDR3seqG=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)

            CDR3seq_all=CDR3seqA[0:]+CDR3seqB[0:]+CDR3seqD[0:]+CDR3seqG[0:]
        else:
            if geneType=='A':
                CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Alpha')
                CDR3seqs=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)
                CDR3seq_all=CDR3seqs[0:]
            if geneType=='B':
                CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Beta')
                CDR3seqs=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)
                CDR3seq_all=CDR3seqs[0:]
            if geneType=='D':
                CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Delta')
                CDR3seqs=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)
                CDR3seq_all=CDR3seqs[0:]
            if geneType=='G':
                CDR3patV, CDR3patJ, VFaDict, JFaDict=GetCDR3Patterns(genome, 'Gamma')
                CDR3seqs=DetectCDR3(Seq,VFaDict,JFaDict,CDR3patV,CDR3patJ)
                CDR3seq_all=CDR3seqs[0:]
    
    BestScore=-1
    BestII=-1
    
    for ii in xrange(0,len(CDR3seq_all)):
        tmp=CDR3seq_all[ii]
        if tmp[-2]>BestScore:
            BestScore=tmp[-2]
            BestII=ii
    
    if BestII==-1:
        #print CDR3seq_all
        return []
    
    count1=0

    CDR3seq=[CDR3seq_all[BestII]]
    
    if len(CDR3seq)==0 or len(CDR3seq)>1:
        return []
    else:
        CDR3=CDR3seq[0]
        if (CDR3[2]==-1 or CDR3[4]==-1) and CDR3[5]==0:
            return []
        tag0=CDR3[0][1]
        if tag0>0:
            offset=tag0-1
        else:
            offset=-tag0-1
        AAseq=CDR3[0][0]
        Vinfo=CDR3[1][0]
        Vgene=''
        if len(Vinfo)>0:
            VgeneList=Vinfo.split('_')
            for VgeneC in VgeneList:
                Vgene+='_'+VgeneC.split('|')[1]
        Vgene=Vgene[1:]
        st=int(CDR3[2])
        Jinfo=CDR3[3][0]
        Jgene=''
        Cgene=''
        Cgeneinfo=None
        if len(Jinfo)>0:
            matchC=re.search('00000', Jinfo)
            if matchC is not None:
                Jgeneinfo=Jinfo.split('00000')[0]
                Cgeneinfo=Jinfo.split('00000')[1]
            else:
                Jgeneinfo=Jinfo
            JgeneList=Jgeneinfo.split('_')
            if len(JgeneList)>0:
                for JgeneC in JgeneList:
                    tmp=JgeneC.split('|')
                    if len(tmp)>1:
                        Jgene+='_'+tmp[1]
                Jgene=Jgene[1:]
            if Cgeneinfo is not None:
                CgeneList=Cgeneinfo.split('_')
                if len(CgeneList)>0:
                    for CgeneC in CgeneList:
                        tmp=CgeneC.split('|')
                        if len(tmp)>1:
                            Cgene+='_'+tmp[1]
                    Cgene=Cgene[1:]
            Jgene+='|'+Cgene
        ed=int(CDR3[4])

        if st>=ed and len(CDR3[3][0])>0:
            #print('erroneous assembly due to repetitive sequence or sequence error')
            return []

        cdr3_seq=''
        seq_comp=ReverseCompSeq(Seq)
        
        if st>=0 and ed>=0:

            ed=ed+3
            if(ed >= len(AAseq)):
                ed=len(AAseq)-1

            cdr3_seq=AAseq[st:(ed+1)]
            if tag0>0:
                DNA_seq=Seq[(offset+st*3):(offset+(ed+1)*3)]
            else:
                DNA_seq=seq_comp[(offset+st*3):(offset+(ed+1)*3)]
        
        if st>=0 and ed==-1:

            cdr3_seq=AAseq[st:]
            if len(cdr3_seq)>=9:#
                match=re.search('[FWLV]G.$',cdr3_seq)   
                if match:
                    cdr3_seq=cdr3_seq[:match.start()+1]
            if len(cdr3_seq)>=13:#
                match=re.search('[FW]G$',cdr3_seq)   
                if match:
                    cdr3_seq=cdr3_seq[:match.start()+1]
        
            ed_new=offset+st*3+len(cdr3_seq)*3

            if tag0>0:
                DNA_seq=Seq[(offset+st*3):ed_new]
            else:
                DNA_seq=seq_comp[(offset+st*3):ed_new]
        
        if ed>=0 and st==-1:

            ed=ed+3
            if(ed >= len(AAseq)):
                ed=len(AAseq)-1

            cdr3_seq=AAseq[:(ed+1)]

            if tag0>0:
                DNA_seq=Seq[offset:(offset+(ed+1)*3)]
            else:
                DNA_seq=seq_comp[offset:(offset+(ed+1)*3)]
        
        if st>=0 and not cdr3_seq.startswith('C'):
            v_trim=re.search('C', cdr3_seq)
            if v_trim:
                if v_trim.start() <= v_trim_start_thro:
                    cdr3_seq=cdr3_seq[v_trim.start():]
                    DNA_seq=DNA_seq[3*v_trim.start():]

        if ed>=0 and not cdr3_seq.endswith('F'):
            
            if Bcell and light_chain and cdr3_seq[-3:]=='TFG':
                cdr3_seq=cdr3_seq[ :(len(cdr3_seq)-1)]
                DNA_seq=DNA_seq[ :3*(len(cdr3_seq))]
            else:
                j_trim=re.search('[FW]', cdr3_seq[-4:])
                if j_trim:
                    cdr3_seq=cdr3_seq[ :(len(cdr3_seq)-3+j_trim.start())]
                    DNA_seq=DNA_seq[ :3*(len(cdr3_seq))]

        if len(DNA_seq)==0:
            ff='NA'
        else:
            DNA_seq_rc=ReverseCompSeq(DNA_seq)
            rr_reads=list(set(ContigReads))
            count1=len(rr_reads)
            count0=0
            tmp_seq_list=[]
            if len(rr_reads)<=2:
                count0=len(rr_reads)
            elif pRD is not None:
                for rr0 in rr_reads:
                    tmp_rr0=pRD[rr0]    
                    if tmp_rr0[0]==1:
                        tmp_seq=tmp_rr0[1][0].seq
                    else:
                        tmp_seq=tmp_rr0[1][1].seq
                    tmp=CompareSuffixSeq(DNA_seq, tmp_seq, err=error, count=True)
                    tmp_seq_list.append(tmp_seq)
                    if tmp[0][0]>=min(overlap_thr,len(DNA_seq)) and tmp[0][1]<3:
                        count0+=1
                    elif DNA_seq in tmp_seq or tmp_seq in DNA_seq or DNA_seq_rc in tmp_seq or tmp_seq in DNA_seq_rc:
                        count0+=1
            if(count0==0): count0=1
            ff=float(count0)/max(len(DNA_seq), dna_len_thro)
        
        S=CDR3[-2]
        m=CDR3[-1]
        
        if m>0:
            mLogE=S-np.log(m)
        else:
            mLogE=0

    return [cdr3_seq,DNA_seq,Vgene,Jgene,ff,mLogE,count1]


