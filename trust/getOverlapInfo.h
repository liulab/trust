/********************************************************************************* 
  *getOverlapInfo.h
  *Author:  JianZhang, jzhang10@foxmail.com
  *Date:  Jan 27, 2017
**********************************************************************************/  

//Calculate the sum of the binary respresentation of a given number
int popcount64c(uint64_t x);

int *compare(uint64_t x, uint64_t y, int n, int* res);

int *compare_Lgreaterthan64(uint64_t *prefix, int ns_pre, int n_pre, uint64_t *suffix, int ns_suf, int n_suf, int CompareSeq, int *res);


