"""
Single end reads proessing
"""


try:
    import pysam
except:
    print '''Please install pysam first: https://code.google.com/p/pysam/'''
    sys.exit()
from Bio import pairwise2

from trust.extract_informative_readpairs import *
from trust.process_seq import *
from trust.find_overlap_seq import *
from trust.build_seq_overlap_matrix import *
from trust.find_disjoint_community import *
from trust.assemble_seqs import *
from trust.detect_cdr3 import *

pairwise2.MAX_ALIGNMENTS=1

def GetGeneDNAPattern(genome='hg19'):

    if genome=='mm10':

        patDict={
                    'BJforward1': re.compile( '[TC][TA][CT]C?TT[CT]GC?G'),
                    'BJforward2': re.compile( 'CCATGGTCTTGG'),
                    'BJforward3': re.compile( 'CTG(AC)?TGATTGG'),
                    'BVforward1': re.compile( 'T[AT]..?T.TGT.?GC'),
                    'BVforward2': re.compile( 'TGCCAG.AG.T'),
                    'BVforward3': re.compile( 'TGTACTTGTGCTCC'),
                    'BVforward4': re.compile( 'TACTTCTGGGCCAG'),
                    'BVforward5': re.compile( 'TGTACTGCACCTGC'),

                    'BJreverse1': re.compile( 'CG?C[AG]AAG?[AG][TA][GA]'),
                    'BJreverse2': re.compile( 'CCAAGACCATGG'),
                    'BJreverse3': re.compile( 'CCAATCA(GT)?CAG'),
                    'BVreverse1': re.compile( 'GC.?ACA.A?..[AT]A'),
                    'BVreverse2': re.compile( 'A.CT.CTGGCA'),
                    'BVreverse3': re.compile( 'GGAGCACAAGTACA'),
                    'BVreverse4': re.compile( 'CTGGCCCAGAAGTA'),
                    'BVreverse5': re.compile( 'GCAGGTGCAGTACA'),

                    'DJforward1': re.compile( 'ACCC?GACA[AG]AC?T'),
                    'DVforward1': re.compile( 'TA[CT][CT][TA][CT]TG[TC][GA]'),

                    'DJreverse1': re.compile( 'AG?T[CT]TGTCG?GGT'),
                    'DVreverse1': re.compile( '[TC][GA]CA[GA][TA][AG][AG]TA')

                }

        patDictAll={
                    'BJforward1': re.compile( '[TC][TA][CT]C?TT[CT]GC?G'),
                    'BJforward2': re.compile( 'CCATGGTCTTGG'),
                    'BJforward3': re.compile( 'CTG(AC)?TGATTGG'),
                    'BVforward1': re.compile( 'T[AT]..?T.TGT.?GC'),
                    'BVforward2': re.compile( 'TGCCAG.AG.T'),
                    'BVforward3': re.compile( 'TGTACTTGTGCTCC'),
                    'BVforward4': re.compile( 'TACTTCTGGGCCAG'),
                    'BVforward5': re.compile( 'TGTACTGCACCTGC'),

                    'BJreverse1': re.compile( 'CG?C[AG]AAG?[AG][TA][GA]'),
                    'BJreverse2': re.compile( 'CCAAGACCATGG'),
                    'BJreverse3': re.compile( 'CCAATCA(GT)?CAG'),
                    'BVreverse1': re.compile( 'GC.?ACA.A?..[AT]A'),
                    'BVreverse2': re.compile( 'A.CT.CTGGCA'),
                    'BVreverse3': re.compile( 'GGAGCACAAGTACA'),
                    'BVreverse4': re.compile( 'CTGGCCCAGAAGTA'),
                    'BVreverse5': re.compile( 'GCAGGTGCAGTACA'),

                    'DJforward1': re.compile( 'ACCC?GACA[AG]AC?T'),
                    'DVforward1': re.compile( 'TA[CT][CT][TA][CT]TG[TC][GA]'),

                    'DJreverse1': re.compile( 'AG?T[CT]TGTCG?GGT'),
                    'DVreverse1': re.compile( '[TC][GA]CA[GA][TA][AG][AG]TA'),

             
                    'GJforward1': re.compile( 'TC[AG]GG[CT]TTTC'),
                    'GJforward2': re.compile( 'TGGGACTTTC'),
                    'GJforward3': re.compile( 'CAGGCACATC'),
        
                    'GVforward1': re.compile( 'GCATGCTGGGA'),
                    'GVforward2': re.compile( 'CTGTGCAGTC'),
                    'GVforward3': re.compile( 'CAG[AT]CTGGA'),
                    'GVforward4': re.compile( 'CTGCTGGGAT'),
                    'GVforward5': re.compile( 'CAACCTTGA'),

                    'GJreverse1': re.compile( 'GAAA[AG]CC[CT]GA'),
                    'GJreverse1': re.compile( 'GAAAGTCCCA'),
                    'GJreverse1': re.compile( 'GATGTGCCTG'),
                    'GVreverse1': re.compile( 'TCCCAGCATGC'),
                    'GVreverse2': re.compile( 'GACTGCACAG'),
                    'GVreverse3': re.compile( 'TCCAG[AT]CTG'),
                    'GVreverse4': re.compile( 'ATCCCAGCAG'),
                    'GVreverse5': re.compile( 'TCAAGGTTG'),
                    

                    'AJforward1': re.compile( 'T.CAAACTT'),
                    'AJforward2': re.compile( 'AACAGACTTA'),
                    'AJforward3': re.compile( 'CT.[AG].[CG].?[TC]TT'),
                    'AJforward4': re.compile( '[AG].?.?[TC]..?TTTGG'),
                    'AJforward4': re.compile('(CTCGGGATA)|(GACTGGAGG)|(ATTCTGGGA)|(ACCAGGGAG)|(ACTTCAAGT)|(CTAACAGTG)|(ATAGAGGTT)|(TATCGAGGT)|(ATACTGGAG)|(ATACTGGAC)|(CTGGTAACT)|(CTAATTACA)|(AATTACAAC)|(CTTCTGGCA)|(ATTATAACC)|(ACTGGCCAG)|(GGACAAAAG)|(ATAACTATG)|(CACCAATAC)|(ACCAGGCAC)|(ATTCAGGAA)|(ATTCAGCTA)|(CACAAATGC)|(ATAGCAATA)|(ATAACAATA)|(ATTATGGGA)|(TTATGGGGG)|(ATAGCAACT)|(TCCAATACC)|(TCCAATACC)|(GACAGGCTT)|(GACAGGGCT)|(ACATTTGAG)|(CAGGCAATA)|(TGTTGGTGA)|(ATAATAATG)|(TATCTGGTA)|(AATACAGGA)|(CTCAAACAC)|(TCTGGAGGA)|(GGAGGAAGC)|(ATAACAACA)|(ATAACAACA)|(ACTGGCAGT)|(ATACAGAAG)|(TACAGGAGG)|(AGACAGCAG)|(ACTATGCAA)|(AACTATGGA)|(CACGGGTTA)|(GACACAGGT)|(CATCCTCCT)|(ACACTGGAG)|(ACAGTGGAG)|(AGAGGCCTG)|(CTACTGGAG)|(ATCAAGGAG)|(AGCAAGGCA)|(CTAAAAAGA)|(CTCAGGAGG)|(GAAGCACTA)|(ACAGAATTA)|(CTACAGCAA)|(AACATGGGC)|(AACATGGGC)'),

                    'AVforward1': re.compile( 'CTG[TC]G[CT]'),
                    'AVforward2': re.compile( 'TTGTGCA' ),
                    'AVforward3': re.compile( 'CTGCTCAG' ),
                    'AVforward4': re.compile( 'CTGTATCCT' ),
                    'AVforward5': re.compile( 'CTGCATTGT' ),
                    
                    'AJreverse1': re.compile( 'AAGTTTG.A'),
                    'AJreverse2': re.compile( 'TAAGTCTGTT'),
                    'AJreverse3': re.compile( 'AA[GA].?[CG].[CT].AG'),
                    'AJreverse4': re.compile( 'CCAAA?..[GA].?.?[CT]'),
                    'AJreverse5': re.compile('(TATCCCGAG)|(CCTCCAGTC)|(TCCCAGAAT)|(CTCCCTGGT)|(ACTTGAAGT)|(CACTGTTAG)|(AACCTCTAT)|(ACCTCGATA)|(CTCCAGTAT)|(GTCCAGTAT)|(AGTTACCAG)|(TGTAATTAG)|(GTTGTAATT)|(TGCCAGAAG)|(GGTTATAAT)|(CTGGCCAGT)|(CTTTTGTCC)|(CATAGTTAT)|(GTATTGGTG)|(GTGCCTGGT)|(TTCCTGAAT)|(TAGCTGAAT)|(GCATTTGTG)|(TATTGCTAT)|(TATTGTTAT)|(TCCCATAAT)|(CCCCCATAA)|(AGTTGCTAT)|(GGTATTGGA)|(AAGCCTGTC)|(AGCCCTGTC)|(CTCAAATGT)|(TATTGCCTG)|(TCACCAACA)|(CATTATTAT)|(TACCAGATA)|(TCCTGTATT)|(GTGTTTGAG)|(TCCTCCAGA)|(GCTTCCTCC)|(TGTTGTTAT)|(ACTGCCAGT)|(CTTCTGTAT)|(CCTCCTGTA)|(CTGCTGTCT)|(TTGCATAGT)|(TCCATAGTT)|(TAACCCGTG)|(ACCTGTGTC)|(AGGAGGATG)|(CTCCAGTGT)|(CTCCACTGT)|(CAGGCCTCT)|(CTCCAGTAG)|(CTCCTTGAT)|(TGCCTTGCT)|(TCTTTTTAG)|(CCTCCTGAG)|(TAGTGCTTC)|(TAATTCTGT)|(TTGCTGTAG)|(GCCCATGTT)'),
                    
                    'AVreverse1': re.compile( '[AG]C[AG]CAG'),
                    'AVreverse2': re.compile( 'TGCAGCAA' ),
                    'AVreverse3': re.compile( 'CTGAGCAG' ),
                    'AVreverse4': re.compile( 'AGGATACAG' ),
                    'AVreverse5': re.compile( 'ACAATGCAG' ),

                    }

        patDictB={  
                    'HJforward1': re.compile( 'TA?[CG]TGGGG[CT].?CA'),
                    'HVforward1': re.compile( 'TA[CT]T[AT][CT]TGT[GA]'),
                    'HVforward2': re.compile( 'CATTTCTGTGCAAG'),
                    'HVforward3': re.compile( 'TACATGCAGCT'),

                    'HJreverse1': re.compile( 'TG.?[AG]CCCCA[CG]?TA'),
                    'HVreverse1': re.compile( '[TC]ACA[AG][AT]A[AG]TA'),
                    'HVreverse2': re.compile( 'CTTGCACAGAAATG'),
                    'HVreverse3': re.compile( 'AGCTGCATGTA')
                  }

        patDictBL={ 
                    'LJforward1': re.compile( 'GGTGTTCGG'),
                    'LJforward2': re.compile( 'T[GA]TTTTCGG'),
                    'LJforward3': re.compile( 'TTCTTTTTC'),
                    'LVforward1': re.compile( 'GTGCAGGCTGA'),
                    'LVforward2': re.compile( 'GTACAGCAACC'),
                    'LVforward3': re.compile( 'TTA[AG]GG[AG]ACAA'),

                    'LJreverse1': re.compile( 'CCGAACACC'),
                    'LJreverse2': re.compile( 'CCGAAAA[CT]A'),
                    'LJreverse3': re.compile( 'GAAAAAGAA'),
                    'LVreverse1': re.compile( 'TCAGCCTGCAC'),
                    'LVreverse2': re.compile( 'GGTTGCTGTAC'),
                    'LVreverse3': re.compile( 'TTGT[CT]CC[CT]TAA'),


                    'KJforward1': re.compile( 'AC[AG]TTC[AG]G'),
                    'KVforward1': re.compile( '[AG]GTACTCCT'),
                    'KVforward2': re.compile( 'AT....?CCT' ),
                    'KVforward3': re.compile( 'AG..[TC]CCT' ),
                    'KVforward4': re.compile( 'AGTATACCT' ),
                    'KVforward5': re.compile( 'AGTTTTCCC' ),
                    'KVforward6': re.compile( 'AGTCTTTCT' ),
                    'KVforward7': re.compile( 'CTTCCT' ),
                    'KVforward8': re.compile( '[AG]GT[AT][AC]CCCA' ),
                

                    'KJreverse1': re.compile( 'C[CT]GAA[CT]GT'),
                    'KVreverse1': re.compile( 'AGGAGTAC[CT]'),
                    'KVreverse2': re.compile( 'AGG....?AT' ),
                    'KVreverse3': re.compile( 'AGG[AG]..CT' ),
                    'KVreverse4': re.compile( 'AGGTATACT' ),
                    'KVreverse5': re.compile( 'AGAAAGACT' ),
                    'KVreverse6': re.compile( 'AGTCTTTCT' ),
                    'KVreverse7': re.compile( 'AGGAAG' ),
                    'KVreverse8': re.compile( 'TGGG[GT][AT]AC[CT]' )
                
                  }
       
    else:
        
        patDict={'BJforward':re.compile('[TC]{1}TT[TC]{1}GG[ATGC]{4}GG'),
                 'BJreverse':re.compile('CC[ATGC]{4}CC[AG]{1}AA[AG]{1}'),
                 'BV1forward':re.compile('[CTG]{1}TG[CT]{1}GCC'),
                 'BV2forward':re.compile('CT[GA]{1}CAG[CT]{1}'),
                 'BV3forward':re.compile('GCCAG[CTA]{1}A'),
                 'BV1reverse':re.compile('GGC[GA]{1}CA[GAC]{1}'),
                 'BV2reverse':re.compile('[GA]{1}CTG[CT]{1}AG'),
                 'BV3reverse':re.compile('T[GAT]{1}CTGGC'),
                 'DJforward':re.compile('T[CT]{1}TT[CT]{1}GG[AC]{1}A[AC]{1}[AGT]{1}GG'),
                 'DJreverse':re.compile('CC[TCA]{1}[TG]{1}T[TG]{1}CC[GA]{1}AA[GA]{1}A'),
                 'DVforward':re.compile('TACT[ACT]{2}TGTGC'),
                 'DVreverse':re.compile('GCACA[TGA]{2}AGTA')}

        patDictAll={'AJforward':re.compile('T[TG]{1}.G[TC]{1}....GG.A'),
                    'AJreverse':re.compile('T.CC....[AG]{1}C.[AC]{1}A'),
                    'AV1forward':re.compile('A...AG..[AC]{1}.[CT]{1}TA'),
                    'AV2forward':re.compile('T.TA[CT]{1}T[AT]{1}CTG'),
                    'AV3forward':re.compile('C..C....TA[CT]{1}.[AT]{1}T'),
                    'AV4forward':re.compile('TGTT.TG'),
                    'AV1reverse':re.compile('TA[GA]{1}.[TG]{1}..CT...T'),
                    'AV2reverse':re.compile('CAG[AT]{1}A[GA]{1}TA.A'),
                    'AV3reverse':re.compile('A[AT]{1}.[GA]{1}TA....G..G'),
                    'AV4reverse':re.compile('CA.AACA'),
                    'BJforward':re.compile('[TC]{1}TT[TC]{1}GG....GG'),
                    'BJreverse':re.compile('CC....CC[AG]{1}AA[AG]{1}'),
                    'BV1forward':re.compile('[CTG]{1}TG[CT]{1}GCC'),
                    'BV2forward':re.compile('CT[GA]{1}CAG[CT]{1}'),
                    'BV3forward':re.compile('GCCAG[CTA]{1}A'),
                    'BV1reverse':re.compile('GGC[GA]{1}CA[GAC]{1}'),
                    'BV2reverse':re.compile('[GA]{1}CTG[CT]{1}AG'),
                    'BV3reverse':re.compile('T[GAT]{1}CTGGC'),
                    'GJforward':re.compile('AA[AG]{1}[CA]{1}[TAC]{1}[CAG]{1}T[CT]{1}[AT]{1}[AG]{1}[GC]{1}'),
                    'GJreverse':re.compile('[GC]{1}[TC]{1}[AT]{1}[GA]{1}A[GTC]{1}[ATG]{1}[GT]{1}[TC]{1}TT'),
                    'GVforward':re.compile('[AG]{1}[TGA]{1}G[CAG]{1}C[TAC]{1}[TC]{1}[GAC]{1}[TAC]{1}G[GA]{1}G'),
                    'GVreverse':re.compile('C[CT]{1}C[GTA]{1}[GCT]{1}[AG]{1}[ATG]{1}G[GTC]{1}C[ACT]{1}[TC]{1}'),
                    'DJforward':re.compile('T[CT]{1}TT[CT]{1}GG[AC]{1}A[AC]{1}[AGT]{1}GG'),
                    'DJreverse':re.compile('CC[TCA]{1}[TG]{1}T[TG]{1}CC[GA]{1}AA[GA]{1}A'),
                    'DVforward':re.compile('TACT[ACT]{2}TGTGC'),
                    'DVreverse':re.compile('GCACA[TGA]{2}AGTA')}

        patDictB={'HJforward':re.compile('CTGGGG[CG]{1}[CA]{1}'),
                  'HJreverse':re.compile('[GT]{1}[CG]{1}CCCCAG'),
                  'HVforward':re.compile('TATTACTGT'),
                  'HVreverse':re.compile('ACAGTAATA')}

        patDictBL={'KJforward':re.compile('GGC...GGGAC'),
                   'KJreverse':re.compile('GTCCC...GCC'),
                   'KVforward':re.compile('CTGA.GAT'),
                   'KVreverse':re.compile('ATC.TCAG'),
                   'LJforward':re.compile('GT.TTCGG'),
                   'LJreverse':re.compile('CCGAA.AC'),
                   'LVforward':re.compile('GA.TA..ACTG'),
                   'LVreverse':re.compile('CAGT..TA.TC')}

    return patDict, patDictAll, patDictB, patDictBL


def ProcessSingleEndReads(fname,LocusFile,HeavyChain=True,light_chain=False,err=1,overlap_thr=10,fasta=True,unmapped=False,Bcell=False, genome='hg19'):

    MotifDict, DNAFaDict, MotifDictB, DNAFaDictB, MotifDictL, DNAFaDictL=GetCDR3Patterns(genome)

    patDict, patDictAll, patDictB, patDictBL=GetGeneDNAPattern(genome)

    CDR3_rr={}
    count=1
    fHandle=pysam.Samfile(fname)
    nr=0

    print '# Extract reads with joining gene DNA motif'
    for rr in fHandle.fetch(until_eof=True):
        
        #print rr.flag&4
        if nr==0:
            nr=rr.rlen

        if 'N' in rr.seq: #or len(set(rr.seq))<=2:
            continue

        if rr.flag & 4 == 0:
            if 'S' in rr.cigarstring:
                score = my_mapping_quality_score(rr.cigartuples, rr.rlen)
                if score >= MAPQ_CUTOFF+0.05:
                    continue
            else:
                continue

        if count % 1000000==0:
            print count

        count+=1        

        if Bcell:
            if not light_chain:
                for geneKey in patDictB:
                    patDNA=patDictB[geneKey]
                    mm=patDNA.findall(rr.seq)
                    if len(mm)>0:
                        if geneKey not in CDR3_rr:
                            CDR3_rr[geneKey]=[rr]
                        else:
                            CDR3_rr[geneKey].append(rr)
                        #break
            else:
                for geneKey in patDictBL:
                    patDNA=patDictBL[geneKey]
                    mm=patDNA.findall(rr.seq)
                    if len(mm)>0:
                        if geneKey not in CDR3_rr:
                            CDR3_rr[geneKey]=[rr]
                        else:
                            CDR3_rr[geneKey].append(rr)
                        #break                
        else:
            if HeavyChain:
                for geneKey in patDict:
                    patDNA=patDict[geneKey]
                    mm=patDNA.findall(rr.seq)
                    if len(mm)>0:
                        if geneKey not in CDR3_rr:
                            CDR3_rr[geneKey]=[rr]
                        else:
                            CDR3_rr[geneKey].append(rr)
                        #break
            else:
                for geneKey in patDictAll:
                    patDNA=patDictAll[geneKey]
                    mm=patDNA.findall(rr.seq)
                    if len(mm)>0:
                        if geneKey not in CDR3_rr:
                            CDR3_rr[geneKey]=[rr]
                        else:
                            CDR3_rr[geneKey].append(rr)
                        #break
    print len(CDR3_rr)
    # Convert DNA sequence into Amino Acid'
    CDR3_AA={}
    for geneKey in CDR3_rr:
        tmp_rr=CDR3_rr[geneKey]
        CDR3_AA[geneKey]=[]
        for rr in tmp_rr:
            CDR3_AA[geneKey].append((rr,TranslateAA(rr.seq)))
    
    # Filter in reads with gene AA motif'
    filteredCDR3_AA={}
    if Bcell:
        if not light_chain:
            MotifDict0=MotifDictB
            DNAFaDict0=DNAFaDictB
        else:
            MotifDict0=MotifDictL
            DNAFaDict0=DNAFaDictL            
    else:
        MotifDict0=MotifDict
        DNAFaDict0=DNAFaDict
    for geneKey in CDR3_AA:
        Motifs=MotifDict0[geneKey[0:2]]
        AApats=Motifs.keys()
        AApats.sort(key=len,reverse=True)
        filteredCDR3_AA[geneKey]=[]
        vv=CDR3_AA[geneKey]
        for tmpAA in vv:
            flag=0
            for AA in tmpAA[1]:
                if '*' in AA[0]:
                    continue
                for pp in AApats:
                    if '*' in pp or pp == '':
                        continue
                    pat=re.compile(pp)
                    tmp=pat.findall(AA[0])
                    if len(tmp)>0:
                        #print pp, AA[0], tmp
                        filteredCDR3_AA[geneKey].append((tmpAA[0],pp,AA))
                        flag=1
                        break
                if flag==1:
                    break
    print 'Filtered CDR3', len(filteredCDR3_AA)


    # Realign kept CDR3 DNA sequences to IMGT reference genes
    MatchedCDR3s={}
    for geneKey in filteredCDR3_AA:
        fCDR3=filteredCDR3_AA[geneKey]
        MatchedCDR3s[geneKey]=[]
        Motifs=MotifDict0[geneKey[0:2]]
        DNAFa=DNAFaDict0[geneKey[0:2]]
        for AA in fCDR3:
            if 'reverse' in geneKey:
                seq=ReverseCompSeq(AA[0].seq)
            else:
                seq=AA[0].seq
            Pats=Motifs[AA[1]]
            matched_aln=[]
            MaxS=-1
            #print "len.Pats", len(Pats)
            for pat in Pats:
                gene=pat.split('|')[1]
                for kk in DNAFa:
                    if gene in kk:
                        break
                SEQ=DNAFa[kk].upper()
                if len(SEQ)>100:    ## For variable genes, only use the 3' end
                    SEQ=SEQ[-45:]

                if USE_PARASAIL:
                    aln = parasail.sw_stats_striped_sat(seq, SEQ, 9, 0, SEQ_SUB) ## need to know length
                    if aln.score >= 0.8 * aln.length and aln.score >= 20:
                        if aln.score > MaxS:
                            MaxS = aln.score
                            matched_aln = [gene, MaxS]
                else:
                    aln=pairwise2.align.localms(seq,SEQ,1,-1,-9,0)
                    if len(aln)==0:
                        continue
                    if aln[0][2]>= 0.8 * (aln[0][4]-aln[0][3]) and aln[0][2]>=20:
                        if aln[0][2]>MaxS:
                            MaxS=aln[0][2]
                            matched_aln=[gene,MaxS]

            if len(matched_aln)>0:
                MatchedCDR3s[geneKey].append((AA[0],AA[2],matched_aln))
        print "fCDR3.len: ", len(fCDR3), geneKey, len(MatchedCDR3s[geneKey])
                    
    # Allocate unmapped SE reads into T/BCR genes
    geneDict={} ## dictionary of dictionary: gene name first, then qname
    rD={}
    pRD={}
    for kk in MatchedCDR3s:
        vv=MatchedCDR3s[kk]
        for v0 in vv:
            g0=v0[2]
            qname=v0[0].qname
            if qname not in rD:
                rD[qname]=[v0[0]]
                pRD[qname]=(1,[v0[0]])
            else:
                qname=qname+'_1'
                rD[qname]=v0[0]
                pRD[qname]=(1,[v0[0]])
            if g0[0] not in geneDict:
                geneDict[g0[0]]={}
                geneDict[g0[0]][qname]=[(v0[0],v0[1],v0[2])]
            else:
                geneDict[g0[0]][qname]=[(v0[0],v0[1],v0[2])]

    # Assemble reads allocated for each gene"
    MasterContig={}
    for kk in geneDict:
        print "---SE: Processing %s." %(kk)
        vv=geneDict[kk]
        print "Reads:", len(vv)
        OI=GetReadsOverlapByGene_SE(vv,nr=nr,err=err,overlap_thr=overlap_thr)
        DC,OD=FindDisjointCommunities(OI)
        if fasta:
            Contigs=[]
        else:
            Contigs=''
        for dc in DC:
            ASs=AssembleCommReads(dc,OD,OI,pRD,rD,nr=nr,thr_overlap=overlap_thr)
            for uu in ASs:
                nk=ASs[uu][2]
                if fasta:
                    Contigs.append(([uu,nk],ASs[uu][0]+ASs[uu][1],ASs[uu][3]))
                else:
                    Contigs+=uu+':'+str(nk)+'_'
        MasterContig[kk]=Contigs

    # Merging contigs
    ContigFinalList=MergeMasterContig(MasterContig,pRD,rD,overlap_thr,err)

    if unmapped:
        # Merge final contigs based on paired-end information
        ContigFinalList=MergeUnmappedPairs(ContigFinalList)

    print "SE: Final contigs assembled", len(ContigFinalList)
    
    # Annotate CDR3 contigs"
    annList=[]
    for cc in ContigFinalList:
        rn=cc[3][0]
        for kk in geneDict:
            if rn in geneDict[kk]:
                v0=geneDict[kk][rn]
                break
        a = cc[2].split('_')
        b = [i[:3] for i in a]
        b = list(set(b))
        if len(b) == 1:
            geneType=b[0][2]
        else:
            geneType=''
        ann=AnnotateCDR3(cc[0],pRD,cc[3],light_chain=light_chain, geneType=geneType,error=err,overlap_thr=overlap_thr,Bcell=Bcell, genome=genome)
        annList.append(ann)

    return annList,ContigFinalList
