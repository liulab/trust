/********************************************************************************* 
  *C++, getOverlapInfo.cpp
  *Author:  JianZhang, jzhang10@foxmail.com
  *Date:  Jan 27, 2017
  *Description: Python C++ extension, compare two integers' binary overlap.
                Input: Two python 64 bit interger lists   
                Output: overlap length, mismatch count, mismatch position
  *Compiler： g++ -fPIC -shared getOverlapInfo.cpp -o getOverlapInfo_C.so -I/usr/include/python2.7/ -lpython2.7
**********************************************************************************/  
#include <Python.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <bitset>

#include "getOverlapInfo.h"

using namespace std;

const uint64_t m1  = 0x5555555555555555; //binary: 0101...
const uint64_t m2  = 0x3333333333333333; //binary: 00110011..
const uint64_t m4  = 0x0f0f0f0f0f0f0f0f; //binary:  4 zeros,  4 ones ...
const uint64_t h01 = 0x0101010101010101; //the sum of 256 to the power of 0,1,2,3...


int popcount64c(uint64_t x)
{
    x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
    x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits 
    x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits 
    return (x * h01) >> 56;  //returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ... 
}


int *compare(uint64_t x, uint64_t y, int n, int* res)
{
    int max_length=0;
    int min_base_count = 10;
    uint64_t tmp1, tmp2;
    int mismatch_count;
    for(int i = n; i >= min_base_count; i--)
    {   //compare length
        tmp1 = y-((y>>i)<<i);//Suffix
        tmp2 = (x>>(n-i))^tmp1;//Prefix
        mismatch_count = popcount64c(tmp2);
        if(mismatch_count == 0)
        {   //perfect match, return
            res[0] = i;//overlap length
            res[1] = mismatch_count;
            res[2] = 0;
            return res;
        }else if(mismatch_count == 1 && max_length == 0)
                {   //max overlap length with 1 base mismatch
                    res[0] = i;//overlap length
                    res[1] = mismatch_count;
                    res[2] = log2(tmp2)+1;//mismatch postion
                    max_length=1;
                } 
/* 
        if(mismatch_count <= 1){//max overlap length with 1 base mismatch
            res[0] = i;//overlap length
            res[1] = mismatch_count;
            if(mismatch_count==0){
                res[2] = 0;
            }else{
                res[2] = log2(tmp2)+1;//mismatch postion
            }
            return res;
        }
*/
    }

    return res;
}


int *compare_Lgreaterthan64(uint64_t *prefix, int ns_pre, int n_pre, uint64_t *suffix, int ns_suf, int n_suf, int CompareSeq, int *res)
{
    int n = fmin(ns_pre, ns_suf);
    //if(CompareSeq == 1)
    //{
    //    n = n-3;
    //}
    //printf("n=%d, n_pre=%d, n_suf=%d\n", n, n_pre, n_suf);
    if(0){
        for(int i = 0; i<n_pre; i++)
        {
            printf("pre = %llu\n", prefix[i]);
            cout<<bitset<64>(prefix[i])<<endl;
        }
        for(int i = 0; i<n_suf; i++)
        {
            printf("suf = %llu\n", suffix[i]);
            cout<<bitset<64>(suffix[i])<<endl;
        }
    }
    int max_length = 0;
    int min_base_count = 10;
    //int OBJmin=9999;
    uint64_t *tmp_pre = new uint64_t[n_pre];
    uint64_t *tmp_suf = new uint64_t[n_suf];
    int mismatch_count;
    for(int i = n; i >= min_base_count; i--){//compare length
        int a,b;
        int count_pre = ceil((ns_pre-i)/64.0);//prefix
        b = ns_pre - i - 64*(count_pre-1);
        for(int j = 0; j < n_pre; j++){
            a = j+count_pre-1;
            if(b == 64){
                tmp_pre[j] = prefix[j+count_pre];
            }else{
                if(j == (n_pre-count_pre)){
                    tmp_pre[j] = prefix[a] >> b;
                    break;
                }else{
                    tmp_pre[j] = (prefix[a] >> b) + (prefix[a+1]<<(64-b));
                }
            }
        }
        int count_suf = ceil(i/64.0);//suffix
        a = i - 64*(count_suf - 1);
        for(int j = 0; j < count_suf; j++){
            if(j == (count_suf-1) && a < 64){
                tmp_suf[j] = suffix[j] - ((suffix[j]>>a)<<a);
                break;
            }else{
                tmp_suf[j] = suffix[j];
            }
        }
        mismatch_count = 0;//overlap
        uint64_t tmp;
        for(int j=0; j<count_suf; j++){
            //printf("Overlap: %d, ", i);
            //printf("%llu,%llu\n", tmp_pre[j],tmp_suf[j]);
            //cout<<bitset<64>(tmp_pre[j])<<","<<bitset<64>(tmp_suf[j])<<endl;
            tmp = tmp_pre[j]^tmp_suf[j];
            mismatch_count += popcount64c(tmp);
        }
        //printf("Mismatch_Count: %d\n", mismatch_count);

        if(mismatch_count == 0){//perfect match, return
            res[0] = i;//overlap length
            res[1] = mismatch_count;
            res[2] = 0;
            delete[]tmp_pre;
            delete[]tmp_suf;
            return res;
        }else if(mismatch_count == 1 && max_length == 0){//max overlap length with 1 base mismatch
                res[0] = i;//overlap length
                res[1] = mismatch_count;
                max_length = 1;
                for(int j=0; j<count_suf; j++){//mismatch postion
                    tmp = tmp_pre[j]^tmp_suf[j];
                    if(popcount64c(tmp) == 1){
                        res[2] = log2(tmp)+64*j+1;
                        break;
                    }
                }
            }
/*
        if(mismatch_count <= 1){//max overlap length tolerate 1 base mismatch
            res[0] = i;//overlap length
            res[1] = mismatch_count;
            if(mismatch_count == 0){
                res[2] = 0;  
            }else{
                for(int j=0; j<count_suf; j++){//mismatch postion
                    tmp = tmp_pre[j]^tmp_suf[j];
                    if(popcount64c(tmp) == 1){
                        res[2] = log2(tmp)+64*j+1;
                        break;
                    }
                }
            }
            delete[]tmp_pre;
            delete[]tmp_suf;
            return res;
        }
*/
    }
    delete[]tmp_pre;
    delete[]tmp_suf;

    return res;
}


static PyObject * _compare(PyObject *self, PyObject *args)
{
    
    uint64_t _x, _y;
    int n;
    int res[3]={-1,-1,-1};
    if (!PyArg_ParseTuple(args, "KKi", &_x, &_y, &n))
        return NULL;
    compare(_x, _y, n, res);
    return Py_BuildValue("iii", res[0], res[1], res[2]);
}


static PyObject * _compare_Lgreaterthan64(PyObject *self, PyObject *args)
{
    PyObject *obj_x, *obj_y;
    int ns_pre, ns_suf, CompareSeq;
    if (!PyArg_ParseTuple(args, "OiOii", &obj_x, &ns_pre, &obj_y, &ns_suf, &CompareSeq))
        return NULL;
    int n_pre = ceil(ns_pre/64.0);
    int n_suf = ceil(ns_suf/64.0);
    uint64_t *_x = new uint64_t[n_pre];
    uint64_t *_y = new uint64_t[n_suf];
    PyObject *obj;
    for(int i =0; i<n_pre;i++){
        obj = PyList_GetItem(obj_x, i);
        _x[i] = PyInt_AsUnsignedLongLongMask(obj);
    }

    for(int i =0; i<n_suf;i++){
        obj = PyList_GetItem(obj_y, i);
        _y[i] = PyInt_AsUnsignedLongLongMask(obj);
    }
    int res[3]={-1,-1,-1};
    compare_Lgreaterthan64(_x, ns_pre, n_pre, _y, ns_suf, n_suf, CompareSeq, res);
    if(res[1] > 1){
        res[0] = -1;
    }
    //printf("%d\n%d\n%d\n",res[0], res[1], res[2]);
    delete[]_x;
    delete[]_y;
    return Py_BuildValue("iii", res[0], res[1], res[2]);
}


static PyMethodDef GreateModuleMethods[] = {
    {
        "compare",
        _compare,
        METH_VARARGS,
        ""
    },
    {
        "compare_Lgreaterthan64",
        _compare_Lgreaterthan64,
        METH_VARARGS,
        ""
    },
    {NULL, NULL, 0, NULL}
};


extern "C"

void initgetOverlapInfo_C()
{
    (void) Py_InitModule("getOverlapInfo_C", GreateModuleMethods);
}

